# Kuopio data challenge project

**Requirements:**

- matplotlib==3.8.1
- numpy==1.26.1
- scipy==1.11.3

To install package with pip, run:
pip install -r req.txt


**Kuopio data challenge:**

Our submission contains the simple difference reconstruction provided by the organizers. We used the code (see https://zenodo.org/records/8252370) and added a multi-bang and TV regularized segmentation step on top in order to get an improved segmentation. The idea for the multi-bang regularization is based on the paper "Convex Regularization of Discrete-Valued Inverse Problems" by Christian Clason &  Thi Bich Tram Do, see https://link.springer.com/chapter/10.1007/978-3-319-70824-9_2

For an exemplary reconstruction, just run Main_TrainingData.py  

and compare your results with our results located in the Outputfolder and plotted in ExamplaryPlots_MB-TV-segmentation.jpg.


**Participants:**


- Patrick Horn, Universität Hamburg, patrick.horn@studium.uni-hamburg.de
- Christina Brandt, Universität Hamburg, christina.brandt@uni-hamburg.de
- Tram Do,  Technische Universität München, tram.do@tum.de
