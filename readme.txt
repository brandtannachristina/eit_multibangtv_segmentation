
Requirements:

matplotlib==3.8.1
numpy==1.26.1
scipy==1.11.3

To install package with pip, run:
pip install -r req.txt


Kuopio data challenge:
Our submission contains the simple difference reconstruction provided by the organizers. We added a multi-bang and TV regularized segmentation step on top in order to get an improved segmentation.

For an exemplary reconstruction, just run Main_TrainingData.py



Participants:
* Patrick Horn, Universität Hamburg, patrick.horn@studium.uni-hamburg.de
* Christina Brandt, Universität Hamburg, christina.brandt@uni-hamburg.de
* Tram Do,  Technische Universität München, tram.do@tum.de
